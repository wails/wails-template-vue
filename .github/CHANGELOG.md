# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Add watcher in `dev` mode

## [v2.0.0-beta.4] - 2022-02-23

### Added

- Add typescript template option by [@misitebao](https://github.com/misitebao)

### Changed

- Modify window size by [@crushonyou18](https://github.com/crushonyou18)

**Full Changelog**: [v2.0.0-beta.3...v2.0.0-beta.4](https://github.com/misitebao/wails-template-vue/compare/v2.0.0-beta.3...v2.0.0-beta.4)
